<?php
	// The commands
	$output = '';

	
	$commands = array(
		'php /home/test/relang/deploy.php',
		'php /home/test/relang/deploy.php',
		'php /home/test/relang/deploy.php'
	);
	// Run the commands for output
	foreach($commands AS $command){
		// Run it
		// $tmp = shell_exec($command);

		shell_exec($command, $out, $status);
		if (0 === $status) {
		    $tmp =  $out ;
		} else {
		    $tmp =  "Command failed with status: $status";
		}

		// Output
		$output .= "<span style=\"color: #6BE234;\">\$</span> <span style=\"color: #729FCF;\">{$command}\n</span>";
		$output .= htmlentities(trim($tmp)) . "\n";
	}
	// Make it pretty for manual user access (and why not?)
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>GIT DEPLOYMENT SCRIPT</title>
</head>
<body style="background-color: #000000; color: #FFFFFF; font-weight: bold; padding: 0 10px;">
<pre>
 ____________________________
|                            |
| Git Deployment Script v0.1 |
|      github.com/riodw 2017 |
|____________________________|

<?php echo $output; ?>
</pre>
</body>
</html>